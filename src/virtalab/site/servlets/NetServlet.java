package virtalab.site.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import virtalab.easyjetty.core.servlets.CoreServlet;

public class NetServlet extends CoreServlet {

	private static final long serialVersionUID = 8431491291779326040L;

	protected void goGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException{
		String br = "<BR>";
		StringBuffer html = new StringBuffer();

		html.append("<h1>Network works!</h1>");
		html.append("By seeing this message you can be sure that your network works well."+br);
		html.append("<h3>Some info:</h3>");
		html.append("This web server runs at: "+req.getLocalAddr()+br);
		html.append("Your computer has IP: "+req.getRemoteAddr()+br);

		reply(html.toString(),resp);

	}

}
