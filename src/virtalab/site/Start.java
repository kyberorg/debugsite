package virtalab.site;

import virtalab.easyjetty.WebServer;
import virtalab.easyjetty.app.exceptions.WebServerException;

public class Start {

	public static void main(String[] args){
		try{
		WebServer server = new WebServer();
		server.setServletPackage("virtalab.site.servlets");
		server.addServlet("NetServlet","/");

		server.run();
		}catch(WebServerException e){
			System.out.print("Error starting web server: "+e.getMessage());
		}
	}

}
